#+TITLE: OpenPGP Card store
#+PROPERTY: header-args :tangle yes :exports both

This is an experimental Private Key Store that enables clients to
access and use cryptographic keys stored on OpenPGP Cards.

The operations are only private key operations (decryption, signing,
ECDH derivation).

There is no configuration as this store automatically detects all
cards connected to the system and selects appropriate one based on
client's request.

WARNING: This is not production ready! Most importantly the crate
leaks data in the capability URLs (in production systems all private
data should be encrypted).

* Running

=cargo run -- -H unix://$XDG_RUNTIME_DIR/pks-openpgp-card.sock= will
start the store using given Unix socket file. This is recommended
method for systems that support Unix sockets.

It is also possible to listen on a TCP socket via =-H
tcp://127.0.0.1:3000= syntax.

* Displaying keys

There is an endpoint for listing subkeys supported by the server:

#+BEGIN_SRC sh :results verbatim
  curl --unix-socket $XDG_RUNTIME_DIR/pks-openpgp-card.sock localhost:3000/keys
#+END_SRC

#+RESULTS:
: 59A29DEA8D37388C656863DFB97A1EE09DB417EC # 0006:05033018 S
: F9FE4648F5F4A9CE23BA298860D2F50529E2DE4F # 0006:05033018 E
: DD977564E03AC260B414A1A53B6DFCC964CFEBC4 # 0006:05033018 A
: E7E2B84A36457BEA3F43692DE68BE3B312FA33FC # 0006:15422467 S
: F99A81E09CD8814B571DBF4AEB0BE68CD9CF08F1 # 0006:15422467 E
: 3BA4FE02BF714A7789CB2E0051F23D6C0529CE0A # 0006:15422467 A
: 61BD3FE44D8546BCB6B37802999E44FA385EA9AB # 0006:15422326 E

* Other projects

You may be interested in the [[https://gitlab.com/sequoia-pgp/ssh-agent-pks][SSH Agent]] that uses this server for key
access.
